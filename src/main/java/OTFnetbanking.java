import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by livquik-8 on 17/11/17.
 */
public class OTFnetbanking {
    WebDriver driver;

    public OTFnetbanking(WebDriver driver)
    {
        this.driver = driver;
    }

    By netbanking = By.id("netbankingpay");
    By bank = By.id("allbanks");
    By pay = By.id("netbankingpaynow");
    By card = By.id("ccard_number");
    By name = By.id("cname_on_card");
    By cvv = By.id("ccvv_number");
    By month = By.id("cexpiry_date_month");
    By year = By.id("cexpiry_date_year");
    By paynow = By.id("pay_button");
    By twofrpay = By.xpath("/html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td/input[1]");

    public WebElement netbank()
    {
        return driver.findElement(netbanking);
    }

    public void bank(String b)
    {
        Select bnk = new Select(driver.findElement(bank));
        bnk.selectByVisibleText(b);
    }

    public WebElement pay()
    {
        return driver.findElement(pay);
    }

    public WebElement card()
    {
        return driver.findElement(card);
    }

    public WebElement name()
    {
        return driver.findElement(name);
    }

    public WebElement cvv()
    {
        return driver.findElement(cvv);
    }

    public void month(String m)
    {
        Select mnth = new Select(driver.findElement(month));
        mnth.selectByVisibleText(m);
    }

    public void year(String y)
    {
        Select yr = new Select(driver.findElement(year));
        yr.selectByVisibleText(y);
    }

    public WebElement paynow()
    {
        return driver.findElement(paynow);
    }

    public WebElement twofrpay()
    {
        return driver.findElement(twofrpay);
    }
}
