import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class outletpayment{
    public WebDriver driver;

    @Parameters({"otp","pin","cvv","bankname","ccholdername","netcc1","netcc2","netcc3","netcc4", "netmonth", "netyear","emiccno","emimonth","emiyear","outleturl","amount","terminalid","outname","retailer","outlet","mobileno","billno","src"})
    @Test
    public void testoutletpayment(String otp, String pin, String cvv, String bankname,String ccholdername, String netcc1, String netcc2, String netcc3, String netcc4, String netmonth, String netyear,String emiccno, String emimonth, String emiyear,String outleturl,String amount,String terminalid,String outname,String retailer,String outlet,String mobileno,String billno,String src) throws Exception
    {
        System.setProperty("webdriver.gecko.driver", "//home//sanjayb//geckodriver");
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability("marionette", true);
        driver = new FirefoxDriver(capabilities);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.manage().deleteAllCookies();
        WebDriverWait wait = new WebDriverWait(driver, 60);
        apicall call = new apicall(driver);
        String url;
        String succurl = "https://apptest.quikwallet.com/#2fr/success";
        String expmsg = "Super! Your payment was successful.";
        login l = new login(driver);
        Status s = new Status(driver);
       try {
            url = call.outletrequest(outleturl,amount,terminalid,outname,retailer,outlet,mobileno,billno,src);
            driver.get(url);
            Thread.sleep(10000);
            try {
                l.otp().sendKeys(otp);
                l.login().click();
                Thread.sleep(6000);
            } catch (Exception e) {
                System.out.println(e);
            }
            if(l.pinpresent() == true)
            {
                l.pin().sendKeys(pin);
                l.cont().click();
            }
            Savedcard sc = new Savedcard(driver);
            sc.card().click();
            sc.cvv().sendKeys(cvv);
            sc.pay().click();
            wait.until(ExpectedConditions.presenceOfElementLocated(sc.twofrpay));
            sc.twofrpay().click();
            Thread.sleep(20000);
            String acturl = driver.getCurrentUrl();
            if (acturl.equals(succurl)) {
                String actmsg = s.success();
                Assert.assertEquals(actmsg, expmsg);
                if (true) {
                    System.out.println("Payment through card is successful");
                }
            } else {
                System.out.println("Payment through card is unsuccessful");
            }
        }
        catch (Exception e){
            System.out.println(e);
        }
        try {
            url = call.outletrequest(outleturl,amount,terminalid,outname,retailer,outlet,mobileno,billno,src);
            driver.get(url);
            Thread.sleep(10000);
            try {
                l.otp().sendKeys(otp);
                l.login().click();
            } catch (Exception e) {
                System.out.println(e);
            }
            if (l.pinpresent() == true) ;
            {
                l.pin().sendKeys(pin);
                l.cont().click();
            }
                OTFnetbanking otfn = new OTFnetbanking(driver);
                otfn.netbank().click();
                otfn.bank(bankname);
                otfn.pay().click();
                wait.until(ExpectedConditions.visibilityOfElementLocated(otfn.card));
                otfn.card().sendKeys(netcc1);
                otfn.card().sendKeys(netcc2);
                otfn.card().sendKeys(netcc3);
                otfn.card().sendKeys(netcc4);
                otfn.name().sendKeys(ccholdername);
                otfn.cvv().sendKeys(cvv);
                otfn.month(netmonth);
                otfn.year(netyear);
                otfn.paynow().click();
                wait.until(ExpectedConditions.presenceOfElementLocated(otfn.twofrpay));
                otfn.twofrpay().click();
                Thread.sleep(20000);
                String acturl1 = driver.getCurrentUrl();
                if (acturl1.equals(succurl)) {
                    String actmsg1 = s.success();
                    Assert.assertEquals(actmsg1, expmsg);
                    if (true) {
                        System.out.println("Payment through netbanking is successful");
                    }
                } else {
                    System.out.println("Payment through netbanking is unsuccessful");
                }

            } catch(Exception e){
            System.out.println(e);
            }

        try {
            url = call.outletrequest(outleturl,amount,terminalid,outname,retailer,outlet,mobileno,billno,src);
            driver.get(url);
            Thread.sleep(10000);
            try {
                l.otp().sendKeys(otp);
                l.login().click();
                if(l.pinpresent() == true);
                {
                    l.pin().sendKeys(pin);
                    l.cont().click();
                }
            } catch (Exception e) {
                System.out.println(e);
            }
            OTFemi otfe = new OTFemi(driver);
            if (otfe.isemipresent()) {
                otfe.emi().click();
                wait.until(ExpectedConditions.visibilityOfElementLocated(otfe.number));
                otfe.number().sendKeys(emiccno);
                otfe.month().sendKeys(emimonth);
                otfe.year().sendKeys(emiyear);
                otfe.cvv().sendKeys(cvv);
                otfe.name().sendKeys(ccholdername);
                otfe.pay().click();
                Thread.sleep(30000);
                String acturl2 = driver.getCurrentUrl();
                if (acturl2.equals(succurl)) {
                    String actmsg2 = s.success();
                    //Thread.sleep(25000);
                    Assert.assertEquals(actmsg2, expmsg);
                    if (true) {
                        System.out.println("Payment through emi is successful");
                    }
                } else {
                    System.out.println("Payment through emi is unsuccessful");
                }
            } else
                System.out.println("Emi is not enabled for this partner");

        } catch (Exception e) {
            System.out.println(e);
        }
    }
    @AfterClass
    public void teardown() {
        //close the app
        driver.quit();


    }
}
