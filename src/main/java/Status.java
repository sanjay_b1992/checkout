import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by livquik-8 on 20/11/17.
 */
public class Status {
    WebDriver driver;

    public Status(WebDriver driver)
    {
        this.driver = driver;
    }

    By success = By.xpath("/html/body/div[2]/div/div/div/div/div[2]/div/div[4]/div/div/div/div[1]");

    public String success()
    {
        return driver.findElement(success).getText();
    }
}
