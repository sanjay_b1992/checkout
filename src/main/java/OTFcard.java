
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class OTFcard {
    WebDriver driver;
    public OTFcard(WebDriver driver)
    {
        this.driver = driver;
    }

    By cardnumber = By.id("cardnumber");
    By month = By.id("month");
    By year = By.id("year");
    By name = By.id("cardholder");
    By cvv = By.id("cvv");
    By pay = By.xpath("/html/body/div[2]/div/div/div/div/div[3]/div/div[2]/div/div/div[7]/div/div/form/a");
    By twofrpay = By.xpath("/html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td/input[1]");

    public WebElement cardnumber()
    {
        return driver.findElement(cardnumber);
    }

    public void month(String m)
    {
        Select mnth = new Select(driver.findElement(month));
        mnth.selectByVisibleText(m);
    }

    public void year(String y)
    {
        Select yr = new Select(driver.findElement(year));
        yr.selectByVisibleText(y);
    }

    public WebElement name()
    {
        return driver.findElement(name);
    }

    public WebElement cvv()
    {
        return driver.findElement(cvv);
    }

    public WebElement pay()
    {
        return driver.findElement(pay);
    }

    public WebElement twofrpay()
    {
        return driver.findElement(twofrpay);
    }
}

