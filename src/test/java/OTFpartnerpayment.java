import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class OTFpartnerpayment {
    public WebDriver driver;

    @Parameters({"ccno","ccmonth","ccyear","ccholdername","cvv","bankname","netcc1","netcc2","netcc3","netcc4","emiccno", "netmonth", "netyear","emimonth","emiyear","partnerurl","mobileno","partnersecret","partnerid","amount"})
    @Test
    public void testOTFpartnerpayment(String ccno, String ccmonth, String ccyear, String ccholdername, String cvv, String bankname, String netcc1, String netcc2, String netcc3, String netcc4, String emiccno, String netmonth, String netyear, String emimonth, String emiyear,String partnerurl,String mobileno,String partnersecret,String partnerid,String amount) throws Exception {
        System.setProperty("webdriver.gecko.driver", "//home//sanjayb//geckodriver");
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability("marionette", true);
        driver = new FirefoxDriver(capabilities);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.manage().deleteAllCookies();
        WebDriverWait wait = new WebDriverWait(driver, 60);
        apicall call = new apicall(driver);
        String url;
        String succurl = "https://apptest.quikwallet.com/#2fr/success";
        String expmsg = "Super! Your payment was successful.";
        login l = new login(driver);
        Status s = new Status(driver);
        try {
            url = call.partnerrequest(partnerurl,mobileno,partnersecret,partnerid,amount);
            driver.get(url);
            Thread.sleep(10000);
            try {
                l.skip().click();
            } catch (Exception e) {
                System.out.println("Skip not located");
            }
            OTFcard otfc = new OTFcard(driver);
            otfc.cardnumber().sendKeys(ccno);
            otfc.month(ccmonth);
            otfc.year(ccyear);
            otfc.name().sendKeys(ccholdername);
            otfc.cvv().sendKeys(cvv);
            otfc.pay().click();
            //Thread.sleep(50000);
            wait.until(ExpectedConditions.presenceOfElementLocated(otfc.twofrpay));
            otfc.twofrpay().click();
            Thread.sleep(20000);
            String acturl = driver.getCurrentUrl();
            if (acturl.equals(succurl)) {
                String actmsg = s.success();
                Assert.assertEquals(actmsg, expmsg);
                if (true) {
                    System.out.println("Payment through card is successful");
                }
            } else
                System.out.println("Payment through card is unsuccessful");
        } catch (Exception e) {
            System.out.println(e);
        }
        try {
            url = call.partnerrequest(partnerurl,mobileno,partnersecret,partnerid,amount);
            driver.get(url);
            Thread.sleep(10000);
            l = new login(driver);
            try {
                l.skip().click();
            } catch (Exception e) {
                System.out.println("Skip not located");
            }
            OTFnetbanking otfn = new OTFnetbanking(driver);
            otfn.netbank().click();
            otfn.bank(bankname);
            otfn.pay().click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(otfn.card));
            otfn.card().sendKeys(netcc1);
            otfn.card().sendKeys(netcc2);
            otfn.card().sendKeys(netcc3);
            otfn.card().sendKeys(netcc4);
            otfn.name().sendKeys(ccholdername);
            otfn.cvv().sendKeys(cvv);
            otfn.month(netmonth);
            otfn.year(netyear);
            otfn.paynow().click();
            wait.until(ExpectedConditions.presenceOfElementLocated(otfn.twofrpay));
            otfn.twofrpay().click();
            Thread.sleep(20000);
            String acturl1 = driver.getCurrentUrl();
            if (acturl1.equals(succurl)) {
                String actmsg1 = s.success();
                Assert.assertEquals(actmsg1, expmsg);
                if (true) {
                    System.out.println("Payment through netbanking is successful");
                }
            } else {
                System.out.println("Payment through netbanking is unsuccessful");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        try {
            url = call.partnerrequest(partnerurl,mobileno,partnersecret,partnerid,amount);
            driver.get(url);
            Thread.sleep(10000);
            l = new login(driver);
            try {
                l.skip().click();
            } catch (Exception e) {
                System.out.println("Skip not located");
            }
            OTFemi otfe = new OTFemi(driver);
            if (otfe.isemipresent()) {
                otfe.emi().click();
                wait.until(ExpectedConditions.visibilityOfElementLocated(otfe.number));
                otfe.number().sendKeys(emiccno);
                otfe.month().sendKeys(emimonth);
                otfe.year().sendKeys(emiyear);
                otfe.cvv().sendKeys(cvv);
                otfe.name().sendKeys(ccholdername);
                otfe.pay().click();
                Thread.sleep(30000);
                String acturl2 = driver.getCurrentUrl();
                if (acturl2.equals(succurl)) {
                    String actmsg2 = s.success();
                    //Thread.sleep(25000);
                    Assert.assertEquals(actmsg2, expmsg);
                    if (true) {
                        System.out.println("Payment through emi is successful");
                    }
                } else {
                    System.out.println("Payment through emi is unsuccessful");
                }
            } else
                System.out.println("Emi is not enabled for this partner");

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @AfterClass
    public void teardown() {
        //close the app
        driver.quit();


    }
}
