import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;


public class login
{
    public WebDriver driver;
    By otp = By.id("otp");
    By resendotp = By.id("resendotp");
    By login = By.id("loginbutton");
    By skip = By.id("skip");
    By pin = By.id("pin");
    By cont = By.id("continuetomodes");

    public login(WebDriver driver)
    {
        this.driver = driver;
        //WebDriverWait wait = new WebDriverWait(driver, 20);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public WebElement otp()
    {
        return driver.findElement(otp);
    }
    public WebElement resendotp()
    {
        return driver.findElement(resendotp);
    }
    public WebElement login()
    {
        return driver.findElement(login);
    }
    public WebElement skip()
    {
        return driver.findElement(skip);
    }
    public boolean pinpresent()
    {
        boolean present;
        present = driver.findElements(pin).size()>0;
        return present;
    }
    public WebElement pin()
    {
        return driver.findElement(pin);
    }
    public WebElement cont()
    {
        return driver.findElement(cont);
    }

}
