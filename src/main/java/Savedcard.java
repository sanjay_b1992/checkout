import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Savedcard {
    WebDriver driver;
    public Savedcard(WebDriver driver)
    {
        this.driver = driver;
    }

    By card = By.xpath("//*[@id=\"ui-accordion-1-header-0\"]");
    By cvv = By.id("cvvfor0");
    By pay = By.xpath("/html/body/div[2]/div/div/div/div/div[3]/div/div[2]/div/div/div[5]/div/div[1]/div/div/form/div/div[2]/a");
    By twofrpay = By.xpath("/html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td/input[1]");

    public WebElement card()
    {
        return driver.findElement(card);
    }

    public WebElement cvv()
    {
        return driver.findElement(cvv);
    }

    public WebElement pay()
    {
        return driver.findElement(pay);
    }

    public WebElement twofrpay()
    {
        return driver.findElement(twofrpay);
    }

}
