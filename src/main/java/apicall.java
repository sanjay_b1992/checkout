
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.lang.reflect.Type;


public class apicall extends partnerreq
{

    WebDriver driver;
    public apicall(WebDriver driver)
    {
        this.driver = driver;
    }


    public String partnerrequest(String url, String mobile3, String partnersecret, String partnerid,String amt) throws ClientProtocolException, IOException {
        String posturl = url;
        Gson gson = new Gson();
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(posturl);
        StringEntity postingString = new StringEntity(gson.toJson(new partnerreq(mobile3,partnersecret,partnerid,amt)));//gson.tojson() converts your pojo to json
        post.setEntity(postingString);
        post.setHeader("Content-type", "application/json");
        org.apache.http.HttpResponse response = httpClient.execute(post);

        HttpEntity httpEntity = response.getEntity();
        String responseString = EntityUtils.toString(httpEntity, "UTF-8");
        System.out.println(responseString);
        Type responseType = new TypeToken<Response>() {}.getType();
        Response responseData = gson.fromJson(responseString, responseType);
        //String status = responseData.getStatus();
        return responseData.getData().getUrl();
    }

    public String outletrequest(String url,String amt, String tid, String name1, String retailer1, String outlet1, String mobile1,String billno, String src) throws Exception
    {

        String posturl = url;
        System.out.println(posturl);
        Gson gson = new Gson();
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(posturl);
        StringEntity postingString = new StringEntity(gson.toJson(new outletreq(amt,tid,name1,retailer1,outlet1,mobile1,billno,src)));//gson.tojson() converts your pojo to json
        post.setEntity(postingString);
        post.setHeader("Content-type", "application/json");
        org.apache.http.HttpResponse response = httpClient.execute(post);

        HttpEntity httpEntity = response.getEntity();
        String responseString = EntityUtils.toString(httpEntity, "UTF-8");
        System.out.println(responseString);
        Type responseType = new TypeToken<Response>() {}.getType();
        Response responseData = gson.fromJson(responseString, responseType);
        //String status = responseData.getStatus();
        return responseData.getData().getLink();
    }
    public String overlaypartnerrequest(String url, String mobile2, String overlaysecret, String overlaypartnerid, String amt, String outletid) throws ClientProtocolException, IOException {
        String posturl = url;
        Gson gson = new Gson();
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(posturl);
        StringEntity postingString = new StringEntity(gson.toJson(new overlaypartnerreq(mobile2, overlaysecret,overlaypartnerid,amt,outletid)));//gson.tojson() converts your pojo to json
        post.setEntity(postingString);
        post.setHeader("Content-type", "application/json");
        org.apache.http.HttpResponse response = httpClient.execute(post);

        HttpEntity httpEntity = response.getEntity();
        String responseString = EntityUtils.toString(httpEntity, "UTF-8");
        System.out.println(responseString);
        Type responseType = new TypeToken<Response>() {}.getType();
        Response responseData = gson.fromJson(responseString, responseType);
        //String status = responseData.getStatus();
        return responseData.getData().getLink();
    }
}