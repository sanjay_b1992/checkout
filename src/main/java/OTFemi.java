import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class OTFemi {
    WebDriver driver;

    public OTFemi(WebDriver driver)
    {
        this.driver = driver;
    }

    By emi = By.id("emipay");
    By number = By.name("card_number_all");
    By month = By.id("mm1");
    By year = By.id("yy1");
    By cvv = By.name("card_cvv");
    By name = By.name("name_on_card");
    By pay = By.xpath("/html/body/div[1]/div[2]/div/form/div/div[2]/div[1]/div[2]/div/div/div[2]/div[3]/button");

    public WebElement emi()
    {
        return driver.findElement(emi);
    }

    public WebElement number()
    {
        return driver.findElement(number);
    }

    public WebElement month()
    {
        return driver.findElement(month);
    }

    public WebElement year()
    {
        return driver.findElement(year);
    }

    public WebElement cvv()
    {
        return driver.findElement(cvv);
    }
    public WebElement name()
    {
        return driver.findElement(name);
    }
    public WebElement pay()
    {
        return driver.findElement(pay);
    }

    public boolean isemipresent()
    {
        if(driver.findElement(emi).isDisplayed())
        return true;
                else
                    return false;

    }
}
